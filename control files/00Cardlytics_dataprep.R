# Data preparation step (params$path,"/R_Code/00Argus_dataprep.R"): 
# user should prepare this script to (1) read the raw Argus dataset, 
#                                    (2) create exposure variables and 
#                                    (3) write as RData file
 #rm(list = ls(all = TRUE))
 # options(run.main=TRUE)

################################################################################
############################# Main Function ####################################
################################################################################
{
  
  main_00 <- function(params) {
    
    message(paste0('\n  Loading ',params$filepath,'.csv'))
    message(paste0('  - Memory usage before reading file: ',memory.size(max=FALSE),
                   ' MB'))
    time <- system.time(argus <- fread(paste0(params$filepath,'.csv'),header=T,
                                       verbose=F,showProgress=F,
                                       nrows=params$rows_to_read, sep=','))
    message(paste0('  - Time it took to read dataset as data.table: ',
                   format(round(time[['elapsed']]/60,2)),' mins.'))
    message(paste0('  - Memory usage is: ',memory.size(max=FALSE),' MB'))
    
    # Set all column names to lowercase
    colnames(argus) <- tolower(colnames(argus))

    # Save as RData
    message(paste0('\n  Writing ',params$filepath,'.RData'))
    colnames(argus) <- tolower(colnames(argus))
    save(argus, file=paste0(params$filepath,'.RData'))
    
    message(paste0('\n  Dataset has ',nrow(argus),' rows and ',
                   ncol(argus),' columns.'))
    
    return(argus)
  }
  
}
################################################################################
############################## Run Main Function ###############################
################################################################################
if (getOption('run.main', default=TRUE)) {
  
  # LOAD UTILITY FUNCTIONS
  # (these are normally loaded by the main script - 10SetParameters)
  
  # Function that checks if package is installed.
  # If installed, it loads it. If it isn't, it installs it.
  pkgTest <- function(x) {
    if (!(x %in% installed.packages()[, 'Package'])) {
      tryCatch({
        message(paste0('INSTALLING PACKAGE ',toupper(x)))
        if (x == 'xda') {
          install_github("ujjwalkarn/xda")
        } else {
          install.packages(x, quiet = TRUE)
        }
      }, error = function(e) {
        stop(paste0('  ERROR: ',e))
      })
    } 
    message(paste0('LOADING PACKAGE ',toupper(x)))
    library(x,character.only = TRUE, quietly = TRUE)
  }
  
  # IMPORT PACKAGES
  # (these are normally loaded by the main script - 10SetParameters)
  pkgTest('jsonlite')
  pkgTest('data.table')
  
  path <- 'Z:/NBI_Sales_Effect/Kenny/cardlytics/current_direct'
  params <- fromJSON(file.path(path,'params.json'), simplifyDataFrame=TRUE)
  main_00(params)
  
}